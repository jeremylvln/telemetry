import * as utils from './utils';

import axios from 'axios';

describe('utils', () => {
  describe('#clone', () => {
    it('returns a string', () => {
      expect(utils.clone('12')).toEqual('12');
    });

    it('returns a null', () => {
      expect(utils.clone(null)).toEqual(null);
    });

    it('returns a undefined', () => {
      expect(utils.clone(undefined)).toEqual(undefined);
    });

    it('returns a number', () => {
      expect(utils.clone(12)).toEqual(12);
    });

    it('returns a boolean', () => {
      expect(utils.clone(true)).toEqual(true);
    });

    it('returns a copy of an object', () => {
      expect(
        utils.clone({
          a: 1,
        }),
      ).toEqual({
        a: 1,
      });
    });

    it('returns a new instance of an object', () => {
      const a = { a: 1 };
      const b = utils.clone(a);

      b.a = 2;

      expect(b).toEqual({
        a: 2,
      });
      expect(a).toEqual({
        a: 1,
      });
    });

    it('returns a copy of an error', () => {
      const err = new Error('Ooops');

      expect(utils.clone(err)).toMatchObject({
        message: 'Ooops',
        stack: err.stack,
      });
    });

    it('returns a copy of an error with sensitive data removed', () => {
      const err = new Error('Ooops');

      // @ts-ignore
      err.details = [
        {
          secret_email: 'john@doe.org',
        },
      ];

      expect(utils.clone(err)).toMatchObject({
        message: 'Ooops',
        stack: err.stack,
        details: [
          {
            secret_email: '>> sensitive <<',
          },
        ],
      });
    });

    it('returns a copy of an error with sensitive data removed not on root level', () => {
      const err = new Error('Ooops');

      // @ts-ignore
      err.details = [
        {
          secret_email: 'john@doe.org',
        },
      ];

      expect(utils.clone({ err })).toMatchObject({
        err: {
          message: 'Ooops',
          stack: err.stack,
          details: [
            {
              secret_email: '>> sensitive <<',
            },
          ],
        },
      });
    });

    it('returns a copy of a nested object with more than MAX_CLONE_LEVEL levels', () => {
      expect(
        utils.clone({
          a: {
            b: {
              c: {
                d: {
                  e: {
                    f: {
                      g: {
                        h: {
                          i: {
                            j: 1,
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        }),
      ).toMatchSnapshot();
    });

    it('returns a copy of an AxiosError', async () => {
      let clonedAxiosError;
      try {
        await axios.request({
          method: 'post',
          url: 'http://localhost:1234/invalid',
          headers: {
            secret_token: 'My Secret  Token',
          },
          params: {
            _q: JSON.stringify({ secret_token: 'my_secret_token' }),
          },
          data: {
            secret_email: 'eve@doe.org',
          },
        });
      } catch (axiosError) {
        clonedAxiosError = utils.clone(axiosError);
      }

      expect(clonedAxiosError).toMatchSnapshot();
    });

    it('returns a copy of an object with invalid JSON stringified object', async () => {
      expect(
        utils.clone({
          data: JSON.stringify({
            secret_email: 'john@doe.org',
          }).slice(0, -2),
        }),
      ).toMatchSnapshot();
    });
  });
});
