import { sdk, opentelemetry } from './opentelemetry';

import logger from './logger';
import * as metrics from './metrics';
import * as helpers from './helpers';
import { metric } from './metrics';

// gracefully shut down the SDK on process exit
process?.on('SIGTERM', () => {
  return stop();
});

/**
 * @deprecated in favor of environment level opentelemetry
 */
function create() {
  return {
    ...opentelemetry,
    logger,
  };
}

async function start() {
  /**
   * initialize the SDK and register with the OpenTelemetry API
   * this enables the API to record telemetry
   */
  try {
    sdk.start();
    logger.debug('[otel] Tracing initialized');
  } catch (err) {
    logger.error('Error initializing tracing', err);
  }
}

function stop() {
  return sdk
    .shutdown()
    .then(() => logger.debug('[otel] Tracing terminated'))
    .catch((err: any) => logger.error('Error terminating tracing', err));
}

export default opentelemetry;

/**
 * @deprecated Prefer using metrics module
 */
export { helpers };

export { create, start, stop, metric, logger, metrics, opentelemetry, sdk };
