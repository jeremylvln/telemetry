import * as opentelemetry from '@opentelemetry/sdk-node';
import { Resource } from '@opentelemetry/resources';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-grpc';
import { PrometheusExporter } from '@opentelemetry/exporter-prometheus';

import { InstrumentationMap } from './constants';
import { MeterProvider } from '@opentelemetry/sdk-metrics';

/**
 * configure the SDK to export telemetry data to the console
 * enable all auto-instrumentations from the meta package
 * @see https://github.com/open-telemetry/opentelemetry-js/tree/main/experimental/packages/exporter-trace-otlp-grpc#readme
 */
const traceExporter =
  process.env.OTEL_TRACES_EXPORTER === 'none'
    ? undefined
    : new OTLPTraceExporter();

const instrumentators = JSON.parse(process.env.OTEL_INSTRUMENTATIONS || '{}');
const instrumentations = Object.keys(instrumentators).map((k) => {
  try {
    if (!InstrumentationMap[k]) {
      throw new Error('Unknown instrumentation');
    }

    const instrumentation = require(k);

    return new instrumentation[InstrumentationMap[k]](instrumentators[k]);
  } catch (err) {
    console.error('[otel] Invalid instrumentation definition', err);
  }
});

const sdk = new opentelemetry.NodeSDK({
  resource: new Resource({
    [SemanticResourceAttributes.SERVICE_NAME]:
      process.env.OTEL_SERVICE_NAME ||
      process.env.TELEMETRY_SERVICE_NAME ||
      'service',
  }),
  traceExporter,
  instrumentations: [instrumentations],
});

const exporter = new PrometheusExporter({
  preventServerStart:
    process.env.OTEL_PROMETHEUS_EXPORTER_PREVENT_SERVER_START === 'true',
});

const meterProvider = new MeterProvider({
  readers: [exporter],
});

export default opentelemetry;
export { sdk, opentelemetry, meterProvider };
