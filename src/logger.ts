import type { Format } from 'logform';

import * as winston from 'winston';
import * as utils from './utils';

function redact(obj: any, opts?: any) {
  if (process.env.TELEMETRY_LOGGER_SANITIZE === 'false') {
    return obj;
  }

  const copy = utils.clone(obj);

  /**
   * @see https://github.com/winstonjs/triple-beam/blob/master/index.js
   */
  copy[Symbol.for('level')] = obj[Symbol.for('level')];
  copy[Symbol.for('message')] = obj[Symbol.for('message')];
  copy[Symbol.for('splat')] = obj[Symbol.for('splat')];

  return copy;
}

function format(opts?: any): Format {
  return {
    transform: (input) => redact(input, opts),
  };
}

export function build(opts?: any): winston.Logger {
  return winston.createLogger({
    level: process.env.TELEMETRY_LOGGER_LEVEL,
    format: winston.format.combine(
      format(opts?.sanitize),
      winston.format.json(opts?.json),
    ),
    defaultMeta: {
      service:
        process.env.OTEL_SERVICE_NAME ||
        process.env.TELEMETRY_SERVICE_NAME ||
        'service',
    },
    transports: [
      new winston.transports.Console({
        format:
          process.env.TELEMETRY_LOGGER_JSON === 'false'
            ? winston.format.simple()
            : undefined,
      }),
    ],
  });
}

const logger = build();

export default logger;
