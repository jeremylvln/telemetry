import * as telemetry from '../src';

import axios from 'axios';

async function main() {
  telemetry.logger.info('Testing logger');

  try {
    await axios.request({
      method: 'post',
      url: 'http://localhost:1234/invalid',
      headers: {
        secret_token: 'My Secret  Token',
      },
      params: {
        _q: JSON.stringify({ secret_token: 'my_secret_token' }),
      },
      data: {
        secret_email: 'eve@doe.org',
      },
    });
  } catch (axiosError) {
    telemetry.logger.error('Axios Error', {
      axios: 'error',
      err: axiosError,
    });

    telemetry.logger.error('Axios Error', axiosError);
  }

  const err = new Error('Ooops');

  // @ts-ignore
  err.details = [
    {
      secret_email: 'john@doe.org',
    },
  ];

  telemetry.logger.error('Error', {
    hello: 'world',
    token: 'token',
    err,
    array: [
      {
        secret_email: 'alice@doe.org',
      },
    ],
  });

  telemetry.logger.error('Error (root)', err);

  await new Promise((resolve) => setTimeout(resolve, 500));

  telemetry.logger.info('Test 2');
}

main().catch((err) => {
  console.error(err);

  process.exit(1);
});
