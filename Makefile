TAG := `cat package.json | jq -r '.version'`

publish: publish_npm

publish_npm:
	npm run build
	npm version patch
	git push --follow-tags
	npm publish
